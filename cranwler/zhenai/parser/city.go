package parser

import (
	"fmt"
	"gitee.com/goTest/cranwler/engine"
	"gitee.com/goTest/cranwler/fetcher"
	"github.com/PuerkitoBio/goquery"
	"github.com/djimenez/iconv-go"
	"log"
	"net/http"
	"time"
)

func ParseCity(url string) engine.ParserResult {
	time.Sleep(time.Second)
	res, _ := http.Get(url)

	defer res.Body.Close()

	charset := fetcher.DetermineEncoding(res.Body)
	utfBody, err := iconv.NewReader(res.Body, charset, "utf-8")
	if err != nil {
		panic(err)
	}
	if res.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(utfBody)

	if err != nil {
		fmt.Printf("city error: %s", err)
	} else {
		result := engine.ParserResult{}

		doc.Find(".shop_list h4 a").Each(func(i int, s *goquery.Selection) {
			username := s.Text()
			url, _ := s.Attr("href")
			result.Items = append(result.Items, username)

			result.Request = append(result.Request, engine.Request{
				Url:       "http:" + url,
				ParserFun: engine.NilParser,
			})
		})
		return result
	}

	return engine.ParserResult{}
}
