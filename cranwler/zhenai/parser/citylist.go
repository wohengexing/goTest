package parser

import (
	"gitee.com/goTest/cranwler/engine"
	"gitee.com/goTest/cranwler/fetcher"
	"github.com/PuerkitoBio/goquery"
	"github.com/djimenez/iconv-go"
	"log"
	"net/http"
)

func ParserCityList(url string) engine.ParserResult {

	res, _ := http.Get(url)

	defer res.Body.Close()
	charset := fetcher.DetermineEncoding(res.Body)
	utfBody, err := iconv.NewReader(res.Body, charset, "utf-8")
	if res.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(utfBody)

	if err != nil {
		log.Fatalf("citylist error: %s", err)
	}
	result := engine.ParserResult{}

	doc.Find(".red").Each(func(i int, s *goquery.Selection) {
		city := s.Text()
		url, _ := s.Attr("href")
		result.Items = append(result.Items, city)

		result.Request = append(result.Request, engine.Request{
			Url:       "http:" + url,
			ParserFun: ParseCity,
		})
	})

	return result
}
