package parser

import (
	"gitee.com/goTest/cranwler/engine"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
)

func ParseUser(url string) engine.ParserResult {
	res, _ := http.Get(url)

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)

	if err != nil {
		panic(err)
	}
	result := engine.ParserResult{}

	doc.Find(".des").Each(func(i int, s *goquery.Selection) {
		username := s.Text()
		url, _ := s.Attr("href")
		result.Items = append(result.Items, username)

		result.Request = append(result.Request, engine.Request{
			Url:       url,
			ParserFun: engine.NilParser,
		})
	})

	return result
}
