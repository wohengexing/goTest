package main

import (
	"gitee.com/goTest/cranwler/engine"
	"gitee.com/goTest/cranwler/zhenai/parser"
)

func main() {

	engine.Run(engine.Request{
		Url:       "http://cd.esf.fang.com/newsecond/esfcities.aspx",
		ParserFun: parser.ParserCityList,
	})
}
