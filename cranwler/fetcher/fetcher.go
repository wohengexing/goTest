package fetcher

import (
	"bufio"
	"golang.org/x/net/html/charset"
	"io"
)

func Fetch(url string) (string, error) {
	return url, nil
}

func DetermineEncoding(r io.Reader) string {
	bytes, err := bufio.NewReader(r).Peek(1024)
	if err != nil {
		return "utf-8"
	}

	_, name, _ := charset.DetermineEncoding(bytes, "")
	return name

}
