package engine

type Request struct {
	Url       string
	ParserFun func(url string) ParserResult
}

type ParserResult struct {
	Request []Request
	Items   []interface{}
}

func NilParser(url string) ParserResult {
	return ParserResult{}
}
