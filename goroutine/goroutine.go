package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

const path = "test.txt"

func main() {
	file, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	for i := 0; i < 1000; i++ {
		go func(i int) {
			defer file.Close()
			writer := bufio.NewWriter(file)
			defer writer.Flush()
			for {
				fmt.Fprintln(writer, i)
			}
		}(i)
	}

	time.Sleep(time.Millisecond)
}
