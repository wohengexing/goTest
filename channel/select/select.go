package main

import "fmt"

func main() {
	var c1, c2 chan int
	select {
	case n := <-c1:
		fmt.Println("Recevied form c1 : %d", n)
	case n := <-c2:
		fmt.Println("Recevied for,m c2 : %d", n)
	default:
		fmt.Println("No value recevied")
	}

}
