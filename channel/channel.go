package main

import (
	"fmt"
	"time"
)

func worker(c chan int) {
	for {
		n := <-c
		fmt.Println(n)
	}
}

func chanDemo() {

	var channels [10]chan int

	for i := 0; i < 10; i++ {
		channels[i] = make(chan int)
		go worker(channels[i])
	}

	for i := 0; i < 10; i++ {
		channels[i] <- i
	}

	time.Sleep(time.Millisecond)
}

func main() {
	chanDemo()
}
