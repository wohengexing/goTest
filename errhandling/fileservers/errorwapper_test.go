package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func errPanic(writer http.ResponseWriter, request *http.Request) error {
	return nil
}

func noError(writer http.ResponseWriter, request *http.Request) error {
	return nil
}

var tests = []struct {
	h       appHandler
	code    int
	message string
}{
	{errPanic, 200, ""},
	{noError, 200, ""},
}

func TestErrorwapper(t *testing.T) {

	for _, tt := range tests {
		f := errorWrapper(tt.h)
		response := httptest.NewRecorder()
		request := httptest.NewRequest(
			http.MethodGet, "http://www.baidu.com", nil)
		f(response, request)
		all, _ := ioutil.ReadAll(response.Body)
		body := string(all)
		if response.Code != tt.code || body != tt.message {
			t.Errorf("expect (%d,%s);got(%d,%s)", tt.code, tt.message, response.Code, body)
		}

	}
}

func TestErrorWrapperInServer(t *testing.T) {
	for _, tt := range tests {
		f := errorWrapper(tt.h)
		server := httptest.NewServer(http.HandlerFunc(f))
		resp, _ := http.Get(server.URL)
		all, _ := ioutil.ReadAll(resp.Body)
		body := string(all)
		if resp.StatusCode != tt.code || body != tt.message {
			t.Errorf("expect (%d,%s);got(%d,%s)", tt.code, tt.message, resp.StatusCode, body)
		}
	}
}
