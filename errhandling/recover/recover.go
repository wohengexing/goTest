package main

import (
	"errors"
	"fmt"
)

func main() {
	defer func() {
		r := recover()
		if error, ok := r.(error); ok {
			fmt.Println("Error occurred :", error)
		} else {
			panic(r)
		}
	}()
	tryRecover()
}

func tryRecover() {
	panic(errors.New("this is a error"))
}
