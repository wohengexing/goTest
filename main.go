package main

import "fmt"

func main() {

	//slices := [...]int{1,2,3,4,5,6,7,8}

	//fmt.Println(getSlices(slices[:]))
	//fmt.Println(slices)

	m := make(map[string]string)
	if k, ok := m["dhdh1"]; ok {
		fmt.Println(k)
	} else {
		fmt.Println("key does not exist")
	}
	delete(m, "dhdh1")

	fmt.Println(m)

	/*for _,v := range maps{

		fmt.Println(v)
	}*/

}

func getSlices(slices []int) []int {
	sli := slices[:4]
	return append(sli, 10)

}
